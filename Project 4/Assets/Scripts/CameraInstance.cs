﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInstance : MonoBehaviour {

	// Use this for initialization
	void Start () {

        if (GameManager.instance.mainCamera == null)	//If we don't already have an instance, make one.
        {
            GameManager.instance.mainCamera = this.GetComponent<Camera>();	// Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject);  //Don't Destroy The GameObject
        }
        else
        {
            Destroy(gameObject);    //Destroy Extra Cameras
        }
    }
}
