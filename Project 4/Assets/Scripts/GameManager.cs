﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //Variables
    public static GameManager instance; //The Game Manager
    public PlayerController player;   //The player
    public GameObject playerpf; //The player prefab
    public Camera mainCamera;   //The Camera
    public Vector3 checkpoint;  //The player's current checkpoint
    public int playerLives; //The player's total lives
    public int playerLivesLeft; //The player's remaining lives
    public AudioClip clip;

    //Runs before start
    void Awake()
    {
        if (instance == null)	//If we don't already have an instance, make one.
        {
            instance = this;	// Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);    //Destroy the extra game manager
            Debug.Log("Warning: A second game manager was detected and destroyed.");	//Warn the designer that they can't add more game managers
        }
    }

    // Use this for initialization
    void Start ()
    {
        playerLivesLeft = playerLives;  //Reset the player's lives
    }

    //Game States

    //The Start Screen
    public void DoStart()
    {
        instance.playerLivesLeft = instance.playerLives;    //On restart, reset lives
        SceneManager.LoadScene(2);  //Load the Start Screen
    }

    //Starting The Game
    public void DoGame()
    {
        SceneManager.LoadScene(0);  //Load level 1
    }

    //When The Player Dies
    public void Death()
    {
        if (playerLivesLeft > 0)    //If the player still has lives
        {
            playerLivesLeft -= 1;   //Take away a life
            Destroy(player.gameObject); //Destroy the player
            Instantiate(playerpf, checkpoint, gameObject.transform.rotation);   //Respawn them
            AudioSource.PlayClipAtPoint(clip, checkpoint, 1f);  //Play the death sound where they respawn
        }
        else
        {
            SceneManager.LoadScene(4);  //Load the Game Over screen
        }
    }
}
