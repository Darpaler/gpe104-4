﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    // Use this for initialization
    public float speed; //The player's speed
    public float xMovement; //Direction in the x axis
    public float jumpForce; //How fast you jump
    public float jumpHeight;    //How high you can jump
    public float yMovement; //Movement in the y axis
    private Transform tf;   //The transform component
    public bool isGrounded = false; //If you're on the ground
    public AudioSource audioSource; //Audio component
    public AudioClip clip1; //Jump Sound
	public int jumpAmount;	//The number of jumps the player has
	public int jumpsLeft;	//How many jumps they have left

    void Start()
    {
        GameManager.instance.player = this; //Set the current instance of the player to this
        tf = GetComponent<Transform>(); //Get the transform component
        audioSource = GetComponent<AudioSource>();  //Get the audiosource component
    }

    // Update is called once per frame
    void Update()
    {
        //Player Movement
        xMovement = Input.GetAxis("Horizontal") * speed;    //There horizontal input at the set speed
        yMovement = Input.GetAxis("Vertical") * jumpForce;  //There jump input * there jump speed
        Rigidbody2D rb2D = GetComponent<Rigidbody2D>(); //The Rigidbody component
        rb2D.velocity = new Vector2(xMovement, rb2D.velocity.y);    //Set velocity to how you move horizontally
        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, Vector2.down, 0.1f, 1 << 8);  //Check if you are grounded

        if (hitInfo.collider != null)   //If theres something under you
        {
            isGrounded = true;  //You're grounded
            jumpsLeft = jumpAmount; //Reset your jumps
        }
        else
        {
            isGrounded = false; //You're not grounded
        }

        if (jumpsLeft > 0)  //If you can still jump
        {
            if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.W))
            {
                jumpsLeft -= 1; //Take away the jump after the stop pressing the button
            }
            if ((Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)) && yMovement < jumpHeight)   //For as long as they hold it, and as long as they don't go higher than their max height
            {
                audioSource.clip = clip1;   //Set the jump sound
                audioSource.Play(); //Play it
                rb2D.velocity = new Vector2(rb2D.velocity.x, yMovement);    //Jump
            }
        }

        //Animations

        if (!isGrounded)    //If you're in the air
        {
            GetComponent<Animator>().Play("PlayerJump");    //You are jumping/falling
        }
        else if (rb2D.velocity.x > 0.1f)    //If you're not in the air and you're moving in a positive direction, youre walking right
        {
            GetComponent<Animator>().Play("PlayerWalk");
            GetComponent<SpriteRenderer>().flipX = false;
        }
        else if (rb2D.velocity.x < -0.1f)   //If you're not in the air and you're moving in a negative direction, youre walking left
        {
            GetComponent<Animator>().Play("PlayerWalk");
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else  //If you're not in the air or moving, than you're idle
        {
            GetComponent<Animator>().Play("PlayerStand");
        }

        //Camera
        GameManager.instance.mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, GameManager.instance.mainCamera.transform.position.z); //Have the camera follow the player

    }
}
