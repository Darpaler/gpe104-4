﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {

    public AudioClip clip;  //The Sound Clip

    private void OnTriggerEnter2D(Collider2D collision)
    {
        AudioSource.PlayClipAtPoint(clip, transform.position, 1f);  //Play the sound at the collitible
        Destroy(gameObject);    //Delete the collectible
    }

}
