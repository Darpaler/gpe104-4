﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour {
 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameManager.instance.Death();   //Kill The Player When Out Of Bounds
    }
}
